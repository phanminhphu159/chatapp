import React, { FC,useState, useEffect,useCallback } from "react"
import { View, ViewStyle, TextStyle, TouchableOpacity, Image, SafeAreaView, LogBox, NativeModules} from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import {
  Text
} from "../../components"
import { color, typography } from "../../theme"
import { MessageNavigatorParamList } from "../../navigators"
import {icons} from '../../components/icon/icons/index'
import { GiftedChat,Bubble } from "react-native-gifted-chat"
import FireMessages from "../../services/firebase/FireMessages"
import {StackActions  } from "@react-navigation/native"
import FireUser from "../../services/firebase/FireUser"
// import ImagePicker from 'react-native-image-crop-picker';
import Images from 'react-native-chat-images'
// import TestPicker from "../../services/firebase/Fire_2"


const FULL: ViewStyle = { flex: 1 }
const TEXT: TextStyle = {
  color: color.palette.white,
  fontFamily: typography.primary,
}
const BOLD: TextStyle = { fontWeight: "bold" }
const TITLE: TextStyle = {
  ...TEXT,
  ...BOLD,
  fontSize: 28,
  lineHeight: 38,
  textAlign: "center",
  color: 'black'
}
const VIEW_HEADER: ViewStyle = {
  flexDirection: 'row',
  marginTop: 0,
  paddingHorizontal: 10,
  borderBottomWidth: 1,
  borderBottomColor: "black",
  alignItems: 'center',
  justifyContent: 'center',
}
LogBox.ignoreLogs(["EventEmitter.removeListener"]);
LogBox.ignoreAllLogs();//Ignore all log notifications



export const MessageChatScreen: FC<StackScreenProps<MessageNavigatorParamList, "message_chat">> = observer(
  ({ navigation,route }) => {

    
    var ImagePicker = NativeModules.ImageCropPicker;
    
    function goBack  ()  { 
      Fire_Messages.off; 
      Fire_User.off; 
      navigation.navigate('user_room',userName);
    };
    const [Messages, setMessages] = useState([]);
    const [image, setImage] = useState(null); 
    const [images, setImages] = useState(null);
    const [userName, setuserName] = useState(null);
    const [userAvatar, setuserAvatar] = useState(null);
    const [imagesURI, setImagesURI] = useState([]);


    const route_params = route.params;
    const userID = route_params.userID;
    const roomID = route_params.roomID;
    const Fire_User = new FireUser();
    const Fire_Messages = new FireMessages();
    

    useEffect(() => {
      async function fetchData() {
        let messages = await Fire_Messages.getMessages(roomID);
        let temp = await Fire_User.getUser(userID);
        setMessages(messages);
        setuserName(temp.name);
        setuserAvatar(temp.avatar);
      }
      fetchData()

      // setMessages([
      //   {
      //     _id: 1,
      //     text: 'Hello',
      //     createdAt: new Date(),
      //     user: {
      //       _id: 2,
      //       name: 'React Native',
      //       avatar: 'https://placeimg.com/140/140/any',
      //     },
      //   },
      //   {
      //     _id: 3,
      //     text: 'Hello developer',
      //     createdAt: new Date(),
      //     images: ["https://picsum.photos/id/237/200/300",'https://placeimg.com/140/140/any','https://placeimg.com/140/140/any2'],
      //     user: {
      //       _id: 2,
      //       name: 'React Native',
      //       avatar: 'https://placeimg.com/140/140/any',
      //     },
      //   },
      //   {
      //     _id: 4,
      //     text: 'Hi',
      //     createdAt: new Date().toString(),
      //     user: {
      //       _id: 1,
      //       name: 'React Native',
      //       avatar: 'https://placeimg.com/140/140/any',
      //     },
      //   },
      // ])

    }, []);

    const renderBubble =(props) => {
      const { currentMessage} = props;
      
      if (currentMessage.images){
        return (
          <View style={{flex:2}}> 
            <Images style={{flex:1}} extra={"currentMessage.createdAt"} backgroundColor="#FFFFFF" images={currentMessage.images} />
            {/* <Images style={{flex:1}} extra={"currentMessage.createdAt"} backgroundColor="#FFFFFF" images={currentMessage.images} /> */}
          </View>
        );
      }

      return <Bubble {...props}/>
    }


    // usecallback: khoi tao ham 1 lan ngay tu lan dau tienen
    const onSendImage = ( image ) => {
      let temp = {
        _id: 5,
        createdAt: new Date(),  
        image: image.map(i => { return i.path;}).toString(),
        // images: ["https://picsum.photos/id/237/200/300",'https://placeimg.com/140/140/any','https://placeimg.com/140/140/any2'],
        user: {
          _id: 2,
          name: 'React Native',
          avatar: 'https://placeimg.com/140/140/any',
        },
      }
      // text.map(t => {
      //   temp = {
      //     _id: '1',
      //     createdAt: new Date(),
      //     image: imagesURI,
      //     user: {
      //       _id: 2,
      //       name: 'React Native',
      //       avatar: 'https://placeimg.com/140/140/any',
      //     },
      //   }
      // }) 
      setMessages(previousMessages => GiftedChat.append(previousMessages, [temp]))
    }
    // , [])


    const onSend = useCallback(( text ) => {
      let temp = text.map(item => {
        if (item.image == undefined || item.image == null)
        {
          return ({
            message: item.text,
            userID: item.user._id,
            roomID: roomID,
          })
        }else {
          return ({
            message: item.text,
            image: item.image,
            userID: item.user._id,
            roomID: roomID,
          })
        }
      });
      Fire_Messages.send(temp)
      // setMessages(previousMessages => GiftedChat.append(previousMessages, text))
    }, [])

    const pickMultiple=() => {
      ImagePicker.openPicker({
          multiple: true,
          waitAnimationEnd: true,
          includeExif: true,
          forceJpg: true,
      }).then(img => {
          setImage(null),
          setImages(img.map(i => {
              return {uri: i.path, width: i.width, height: i.height, mime: i.mime};
          }))
          getImageURI(img); // return ["","",...]
          // {images.map(i => renderAsset(i) )}
      }).catch(e => alert(e));
    }

    const getImageURI = (image) => {
      onSendImage(image)
    }


    return (
      <View testID="WelcomeScreen" style={FULL}>
      {/* // Header Design  */}
      <View style={VIEW_HEADER}>
        <TouchableOpacity onPress={goBack} style={{left: 15, position: 'absolute'}}>
          <Image style={{tintColor: "black", height: 25, width: 25}} resizeMode="cover" source={icons.left} />
        </TouchableOpacity>
        <View style={{}}>
          <Text style={TITLE}>Chat Screen</Text>
        </View>
      </View>
      
     <SafeAreaView style={{flex:1}}>
        {/* <GiftedChat
          renderBubble = {renderBubble}
          messages={Messages}
          onSend={onSend}
          user={{
            _id: 2, 
            name: Name, 
            avatar:'https://placeimg.com/140/140/any'
          }}
        /> */}
        <GiftedChat 
          // showAvatarForEveryMessage={true} 
          showUserAvatar={true} 
          messages={Messages} 
          onSend={onSend} 
          user={{_id: userID, name: userName, avatar: userAvatar }} 
          />
      </SafeAreaView>
      </View>

    )
  },
)

